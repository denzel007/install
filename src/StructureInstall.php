<?php

namespace Tetrapak07\InstallerP;

defined('DS') || define('DS', DIRECTORY_SEPARATOR);
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__). ''.DS.'..'.DS.''.'..'.DS.''));
defined('INSTALL_PATH') || define('INSTALL_PATH', getenv('INSTALL_PATH') ?: realpath(dirname(__FILE__). ''.DS.'..'.DS.'..'.DS.'..'.DS.'..'.DS.''));
defined('INSTALL_VENDOR_CONFIG_FILES') || define('INSTALL_VENDOR_CONFIG_FILES', 0);

use Composer\Script\Event;
# use Composer\Installer\PackageEvent;

/**
 * Description of StructureInstall
 *
 */
class StructureInstall
{
    
    public static function install(Event $event)
    {      
        $extra = $event->getComposer()->getPackage()->getExtra();
        $configInstall = (isset($extra['config-files-install'])&& $extra['config-files-install'] == 'yes') ? true : false;
        $notInstallModules = (isset($extra['no-stucture-install'])) ? $extra['no-stucture-install'] : [];
        $notInstallAllModules = (isset($extra['no-install-all'])&& $extra['no-install-all'] == 'yes') ? true : false;
        
        
        $modules = ['authreg', 'crudgen','emailer','validfiltr','upload','multilang','multitimezone','dashboard','pager',
            'users','search','flashmsg','api','roles','segments','companies','news','gendoc','supermodule', 'baseapi'];
        if ($notInstallAllModules) {
           $notInstallModules =  $modules;
           
        }
        print_r($notInstallModules);    
        $vendorPath = BASE_PATH;
        echo 'Please wait... Structure installing...';
        $vendorDirs = scandir($vendorPath);
        # print_r($vendorDirs);
        foreach ($vendorDirs as $key => $vendorDir) {
            if ((in_array($vendorDir, $modules))&&(!in_array($vendorDir, $notInstallModules))&&!$notInstallAllModules) {
                
                $dir = DS.$vendorDir;
                $structure = BASE_PATH.$dir.DS.'site'.DS;
                //echo $structure;exit;
                $filesAndDirs = self::getDirContents($structure);

                foreach ($filesAndDirs as $value) {

                   if(file_exists($value)){ 

                       $explPath = explode(DS.'site', $value);

                       $newPath = '';
                       if (isset($explPath[1])) {
                           $newPath = INSTALL_PATH.$explPath[1];
                          
                       }

                       if (file_exists($newPath)) {
                          # echo '<br>New pth exist: '.$newPath ; 
                       } else {

                          if (is_dir($value)) {  
                           # echo '<br>New pth NOT exist (DIR): '.$newPath ;  
                            mkdir($newPath, 0755, true);
                          } elseif (is_file($value)) {
                              
                             $pos = stripos($value, "config.$vendorDir.php"); 
                             if ($pos !== false) {
                              #echo ' INSTALL_VENDOR_CONFIG_FILES: '.getenv('INSTALL_VENDOR_CONFIG_FILES');
                              if (! $configInstall) {
                              # if (! getenv('INSTALL_VENDOR_CONFIG_FILES')) {
                                  continue;
                              }
                             
                             }
                            #echo '<br>New pth NOT exist (FILE): '.$newPath ; 

                            if (!copy($value, $newPath)) {
                                echo '<br>copy ERROR!';
                            }

                          } 

                       }
                   }
               }
            }
        }
    }
       
   private static function getDirContents($dir, $onlyFiles = false ,&$results = array())
   {
       $files = scandir($dir);

        foreach($files as $key => $value){
            $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
            if(!is_dir($path)) {
                $results[] = $path;
            } else if($value != "." && $value != "..") {
                self::getDirContents($path, $onlyFiles, $results);
                if (!$onlyFiles) {
                  $results[] = $path;
                }
            }
        }

        return array_reverse($results);
   }
}
